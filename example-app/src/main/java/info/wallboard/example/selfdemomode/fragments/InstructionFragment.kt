package info.wallboard.example.selfdemomode.fragments

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import info.wallboard.example.selfdemomode.R
import info.wallboard.example.selfdemomode.activities.ScannerActivity
import kotlinx.android.synthetic.main.fragment_instruction.*

class InstructionFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_instruction, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        qrCodeResult = resultText

        scannerBtn.setOnClickListener {
            val scannerIntent = Intent(activity, ScannerActivity::class.java)
            startActivity(scannerIntent)
        }
        factoryResetBtn.setOnClickListener {
            val frIntent = Intent(Settings.ACTION_PRIVACY_SETTINGS)
            startActivity(frIntent)
        }
    }

    companion object {
        fun newInstance(): InstructionFragment = InstructionFragment()
        var qrCodeResult: TextView? = null
    }
}