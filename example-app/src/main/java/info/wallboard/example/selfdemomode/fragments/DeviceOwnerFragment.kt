package info.wallboard.example.selfdemomode.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import info.wallboard.example.demo_mode.AppEnability
import info.wallboard.example.selfdemomode.R
import info.wallboard.example.selfdemomode.activities.BootActivity
import info.wallboard.example.selfdemomode.activities.DisplayActivity

@Suppress("DEPRECATION")
class DeviceOwnerFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_deviceowner, container, false)

    companion object {
        fun newInstance(): DeviceOwnerFragment = DeviceOwnerFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        AppEnability.disableApplication(activity!!, BootActivity::class.java)

        val launch = Intent(context, DisplayActivity::class.java)
        launch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context!!.startActivity(launch)
    }
}