package info.wallboard.example.selfdemomode.activities

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GestureDetectorCompat
import info.wallboard.example.demo_mode.AppEnability
import info.wallboard.example.demo_mode.DemoPolicies
import info.wallboard.example.demo_mode.service.DemoService
import info.wallboard.example.selfdemomode.R
import info.wallboard.example.selfdemomode.receiver.DeviceOwnerReceiver
import info.wallboard.pinlockview.callbacks.RemoveCallback
import info.wallboard.pinlockview.view.PinLock

@RequiresApi(Build.VERSION_CODES.N)
class DisplayActivity : AppCompatActivity(), RemoveCallback, GestureDetector.OnGestureListener,
    GestureDetector.OnDoubleTapListener {

    private var isLocked = false
    private var pinLockView: PinLock? = null

    private var adminCompName: ComponentName? = null
    private var demoPolicies: DemoPolicies? = null

    private var isVolumeBtnPressed: Boolean = false

    private var detector: GestureDetectorCompat? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display)
        Log.d(TAG, " onCreate called...")

        supportActionBar?.hide()

        adminCompName = DeviceOwnerReceiver.getComponentName(this)
        demoPolicies = DemoPolicies(this)
        Log.d(TAG, "demoPolicies variable value is = $demoPolicies")

        if (!isLocked) isLocked = true

        if (isLocked) {
            if (adminCompName != null) {
                demoPolicies?.run {
                    this.setDemoPolicies(adminCompName!!, true, this@DisplayActivity)
                }

                Log.d(TAG, " your device LOCKED...")
            }
        }

        detector = GestureDetectorCompat(this, this)
        detector?.run {
            setOnDoubleTapListener(this@DisplayActivity)
        }
    }

    override fun onDestroy() {
        if (pinLockView != null) {
            pinLockView = null
        }
        demoPolicies?.run {
            demoPolicies = null
        }
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, " onResume called...")
        if (isLocked) {
            if (adminCompName != null) {
                demoPolicies?.run { this.setLockTask(adminCompName!!, true, this@DisplayActivity) }
            }
        }
    }

    override fun onBackPressed() {
        // Nothing to do here
        // really...
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        detector?.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            Log.d(TAG, "Volume button is pressed..")
            isVolumeBtnPressed = true
        }
        return true
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            Log.d(TAG, "Volume button is released..")
            isVolumeBtnPressed = false
        }
        return true
    }

    private fun wantToDisableDemoMode() {
        if (isVolumeBtnPressed) {
            if (pinLockView == null) {
                pinLockView = PinLock(this, "1234", this)
            }
            addToWindowManager(pinLockView!!)
        }
    }

    @Suppress("DEPRECATION")
    private fun addToWindowManager(pinLockView: PinLock) {

        val windowManager =
            getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val params =
            WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY else WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT
            )
        params.gravity = Gravity.CENTER

//        try {
//            windowManager.removeView(pinLockView)
//        } catch (e: IllegalArgumentException) {
//            Log.e(TAG, "view not found")
//        }

        windowManager.addView(pinLockView, params)
        pinLockView.bringToFront()
    }

    private fun removeFromWindowManager(pinLockView: PinLock) {
        val windowManager =
            getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.removeViewImmediate(pinLockView)
        this.pinLockView = null
    }

    // PinLock callbacks for result
    override fun onRemove(pinLockView: PinLock) {
        removeFromWindowManager(pinLockView)
    }

    override fun onAccess(pinLockView: PinLock) {
        isLocked = false
        removeFromWindowManager(pinLockView)
        if (adminCompName != null) {
            demoPolicies?.run { this.setDemoPolicies(adminCompName!!, false, this@DisplayActivity) }
        }
        AppEnability.enableApplication(this, BootActivity::class.java)
        Log.d(TAG, " your device UNLOCKED...")
    }


    // GestureListrener overrides ---------------------------------------------------------------------------------------
    override fun onSingleTapUp(p0: MotionEvent?): Boolean {
        Log.d(TAG, "onSingleTapUp Up!")
        return true
    }

    // Unused... but must have
    override fun onShowPress(p0: MotionEvent?) {
        Log.d(TAG, "onShowPress")
    }

    override fun onDown(p0: MotionEvent?): Boolean {
        Log.d(TAG, "onDown")
        return false
    }

    override fun onFling(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float): Boolean {
        return false
    }

    override fun onScroll(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float): Boolean {
        return false
    }

    override fun onLongPress(p0: MotionEvent?) {
        Log.d(TAG, "onDown")
    }

    override fun onDoubleTap(p0: MotionEvent?): Boolean {
        Log.d(TAG, "onDoubleTap")
        wantToDisableDemoMode()
        return true
    }

    override fun onDoubleTapEvent(p0: MotionEvent?): Boolean {
        Log.d(TAG, "onDoubleTapEvent")
        return true
    }

    override fun onSingleTapConfirmed(p0: MotionEvent?): Boolean {
        Log.d(TAG, "onSingleTapComfirmed!")

        if (isLocked) {
            if (adminCompName != null) {
                demoPolicies?.run {
                    this.setLockTask(
                        adminCompName!!,
                        false,
                        this@DisplayActivity
                    )
                }
            }
        }


        val launchIntent = packageManager.getLaunchIntentForPackage(packageName)
        Log.i(TAG, "LAUNCHINTENT IS : $launchIntent")

        val serviceIntent = Intent(this, DemoService::class.java)
        serviceIntent.action = DemoService.START_ACTION
        serviceIntent.putExtra("RootPackage", launchIntent)

        AppEnability.disableApplication(this, BootActivity::class.java)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.startForegroundService(serviceIntent)
        } else {
            this.startService(serviceIntent)
        }
        startActivity(Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME))

        return true
    }
    // --------------------------------------------------------------------------------------------------------------

    companion object {
        private const val TAG = "DisplayActivity"
    }

}




