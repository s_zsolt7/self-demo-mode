package info.wallboard.example.selfdemomode.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class BootUpReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d(TAG, "onReceive called, action is: ${intent?.action}")
//        if (Intent.ACTION_BOOT_COMPLETED == intent?.action) {
//            val appIntent = context?.packageManager!!.getLaunchIntentForPackage(context.packageName)
//            Log.d(TAG, "launcher intent is : $appIntent")
//            context.startActivity(appIntent)
//        }
    }
    companion object {
        private const val TAG = "BOOT_UP_RECEIVER"
    }
}