package info.wallboard.example.selfdemomode.receiver

import android.app.admin.DeviceAdminReceiver
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import info.wallboard.example.selfdemomode.activities.BootActivity
import info.wallboard.example.selfdemomode.activities.DisplayActivity

class DeviceOwnerReceiver : DeviceAdminReceiver() {

    override fun onProfileProvisioningComplete(context: Context, intent: Intent) {
        Log.d(TAG, "onProfileProvisioningComplete : starts")
        val manager: DevicePolicyManager =
            context.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        val componentName: ComponentName =
            getComponentName(
                context
            )

        manager.setProfileName(componentName, "Digital Signage")
        manager.setProfileEnabled(componentName)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            manager.setDeviceOwnerLockScreenInfo(componentName, "This device is belong to Wallboard")
        }

        val playDisplay = Intent(context, BootActivity::class.java)
        playDisplay.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(playDisplay)

        Log.d(TAG, "onProfileProvisioningComplete : ends")
    }


    override fun onLockTaskModeEntering(context: Context, intent: Intent, pkg: String) {
        super.onLockTaskModeEntering(context, intent, pkg)
        Log.d(TAG, "onTaskModeEntering")
    }

    override fun onLockTaskModeExiting(context: Context, intent: Intent) {
        super.onLockTaskModeExiting(context, intent)
        Log.d(TAG, "onTaskModeExiting")
    }

    companion object {
        fun getComponentName(context: Context): ComponentName =
            ComponentName(context.applicationContext, DeviceOwnerReceiver::class.java)

        private val TAG = DeviceOwnerReceiver::class.java.name
    }
}