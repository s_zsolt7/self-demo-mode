package info.wallboard.example.selfdemomode.activities

import android.app.admin.DevicePolicyManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import info.wallboard.example.demo_mode.AppEnability
import info.wallboard.example.selfdemomode.R
import info.wallboard.example.selfdemomode.fragments.DeviceOwnerFragment
import info.wallboard.example.selfdemomode.fragments.InstructionFragment


class BootActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_boot)

        if (savedInstanceState == null) {
            val manager: DevicePolicyManager =
                getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager

            if (manager.isDeviceOwnerApp(applicationContext.packageName)) {
                Log.d(TAG, "The app is the device owner.")
                showFragment(DeviceOwnerFragment.newInstance())
            } else {
                Log.d(TAG, "The app is NOT the device owner.")
                showFragment(InstructionFragment.newInstance())
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()
        if (!Settings.canDrawOverlays(this)) {
            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + this.packageName))
            startActivityForResult(intent, 0)
        }
    }

    override fun onDestroy() {
        AppEnability.enableApplication(this@BootActivity, BootActivity::class.java)
        super.onDestroy()
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commit()
    }

    companion object {
        private const val TAG = "BootActivity"
    }
}
