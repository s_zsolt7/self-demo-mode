package info.wallboard.example.demo_mode

import android.content.ComponentName
import android.content.Context
import android.content.pm.PackageManager

object AppEnability {

    fun <T> enableApplication(pkg: Context, root: Class<T>) {
        val p: PackageManager = pkg.packageManager
        val packageName = ComponentName(pkg, root)
        p.setComponentEnabledSetting(
            packageName,
            PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
            PackageManager.DONT_KILL_APP
        )
    }

    fun <T> disableApplication(pkg: Context, root: Class<T>) {
        val p: PackageManager = pkg.packageManager
        val packageName = ComponentName(pkg, root)
        p.setComponentEnabledSetting(
            packageName,
            PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
            PackageManager.DONT_KILL_APP
        )
    }
}