package info.wallboard.example.demo_mode

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import info.wallboard.example.demo_mode.service.DemoService

class MyAlarm(val context: Context) {
    private var alarmManager: AlarmManager? = null
    private var alarmIntent: PendingIntent? = null

    internal fun startAlarm() {
        if (alarmManager == null) {
            alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        }
        alarmIntent = PendingIntent.getBroadcast(
            context,
            DemoService.ALARM_REQ_CODE, Intent(
                context,
                info.wallboard.example.demo_mode.receiver.TimerReceiver::class.java
            ), PendingIntent.FLAG_CANCEL_CURRENT
        )
        alarmManager!![AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 10000] = alarmIntent
        Log.d(TAG, "startAlarm() called!")
    }

    internal fun cancelAlarm() {
        if (alarmIntent != null) {
            alarmIntent!!.cancel()
            if (alarmManager != null) {
                alarmManager!!.cancel(alarmIntent)
            }
        }
        Log.d(TAG, "cancelAlarm() called!")
    }

    companion object {
        private const val TAG = "MyAlarm"
    }
}