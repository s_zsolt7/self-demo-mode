package info.wallboard.example.demo_mode.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import info.wallboard.example.demo_mode.service.DemoService

class TimerReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        Log.d(TAG, "TimerReceiver received...")

        if (!DemoService.exitOnTouch) {
            val serviceIntent = Intent(context, DemoService::class.java)
            serviceIntent.action = DemoService.STOP_ACTION

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(serviceIntent)
            } else {
                context.startService(serviceIntent)
            }
        }

//        val openApp = Intent(context, DisplayActivity::class.java)
//        context.startActivity(openApp.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
    }

    companion object {
        private const val TAG = "TimerReceiver"
    }
}