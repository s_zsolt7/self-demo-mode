package info.wallboard.example.demo_mode.service

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.opengl.Visibility
import android.os.Build
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import androidx.core.app.NotificationCompat
import info.wallboard.example.demo_mode.BuildConfig
import info.wallboard.example.demo_mode.MyAlarm

class DemoService : Service(), View.OnTouchListener {

    private var windowManager: WindowManager? = null
    private var dummyView: LinearLayout? = null

    private var alarm: MyAlarm? = null

    private var launchIntent: Intent? = null

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onCreate() {
        super.onCreate()
        setUpNotificationChannel()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val action = intent.action
        if (action != null) {
            when (action) {
                START_ACTION -> {
                    launchIntent = intent.getParcelableExtra("RootPackage")
                    Log.d(TAG, "LaunchIntent is : $launchIntent !")
                    Log.d(TAG, "START_ACTION commanded...")
                    if (alarm == null) {
                        alarm = MyAlarm(this)
                    }

                    addDummyView()
                }
                STOP_ACTION -> {
                    Log.d(TAG, "STOP_ACTION commanded...")

                    removeDummyView()
                    launchIntent?.run {
                        Log.d(TAG, "launching launcher Intent....")
                        startActivity(this)
                    }
                    stopSelf()
                }
            }
        }
        return START_NOT_STICKY
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
        alarm?.run {
            this.startAlarm()
        }
        return false
    }

    private fun canDrawOverlay(): Boolean {
        return when {
            Build.VERSION.SDK_INT > Build.VERSION_CODES.M -> true
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> Settings.canDrawOverlays(this)
            else -> return false.also { Log.d(TAG, "Cannot draw overlay!") }
        }
    }

    @Suppress("DEPRECATION")
    @SuppressLint("ClickableViewAccessibility")
    private fun addDummyView() {
        windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        if (canDrawOverlay()) {
            dummyView = LinearLayout(this)

            val params =
                WindowManager.LayoutParams(1, ViewGroup.LayoutParams.MATCH_PARENT)
            dummyView!!.layoutParams = params
            dummyView!!.setOnTouchListener(this)

            val windowParams = WindowManager.LayoutParams(
                1,
                1,
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
                else
                    WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT

            )

            windowParams.gravity = Gravity.START or Gravity.TOP
            windowManager?.addView(dummyView, windowParams)

            Log.d(TAG, "DummyView put down!")
        }
        alarm?.run {
            this.startAlarm()
        }
    }

    private fun removeDummyView() {
        dummyView?.run {
            windowManager!!.removeView(dummyView)
            Log.d(TAG, "DummyView removed!")
        }
        alarm?.run {
            this.cancelAlarm()
        }
    }

    private fun setUpNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val intent = Intent(this, DemoService::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val notification: Notification =
                Notification.Builder(
                    this,
                    createNotificationChannelForService(notificationManager)
                )
                    .setOngoing(false)
                    .setAutoCancel(true)
                    .setContentTitle("DemoNotification")
                    .setContentIntent(pendingIntent)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .build()
            startForeground(1, notification)
        }
    }

    @SuppressLint("NewApi")
    private fun createNotificationChannelForService(notificationManager: NotificationManager): String {
        val channel = NotificationChannel(
            NOTIFICATION,
            "DemoNotificationChannel",
            NotificationManager.IMPORTANCE_NONE
        )

        channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        notificationManager.createNotificationChannel(channel)

        return NOTIFICATION
    }


    companion object {
        private const val TAG = "DemoService"
        private const val NOTIFICATION = "DemoServiceNotification"
        const val ALARM_REQ_CODE = 4289

        const val START_ACTION = BuildConfig.LIBRARY_PACKAGE_NAME + ".demo.START"
        const val STOP_ACTION = BuildConfig.LIBRARY_PACKAGE_NAME + ".demo.STOP"

        @Volatile
        var exitOnTouch = false
    }
}