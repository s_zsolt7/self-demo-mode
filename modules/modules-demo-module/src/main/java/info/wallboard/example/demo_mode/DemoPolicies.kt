package info.wallboard.example.demo_mode

import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.ContentValues.TAG
import android.content.Context
import android.os.Build
import android.os.UserManager
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity

@RequiresApi(Build.VERSION_CODES.N)
class DemoPolicies(val context: Context) {

    private val defaultRestrictions: ArrayList<String> = arrayListOf(
        UserManager.DISALLOW_ADD_USER,
        UserManager.DISALLOW_ADJUST_VOLUME,
        UserManager.DISALLOW_APPS_CONTROL,
        UserManager.DISALLOW_CONFIG_BLUETOOTH,
        UserManager.DISALLOW_CONFIG_CREDENTIALS,
        UserManager.DISALLOW_CONFIG_MOBILE_NETWORKS,
        UserManager.DISALLOW_CONFIG_TETHERING,
        UserManager.DISALLOW_CONFIG_VPN,
        UserManager.DISALLOW_CONFIG_WIFI,
        UserManager.DISALLOW_DATA_ROAMING,
//        UserManager.DISALLOW_FACTORY_RESET,
//        UserManager.DISALLOW_INSTALL_APPS,
        UserManager.DISALLOW_INSTALL_UNKNOWN_SOURCES,
        UserManager.DISALLOW_MODIFY_ACCOUNTS,
        UserManager.DISALLOW_MOUNT_PHYSICAL_MEDIA,
        UserManager.DISALLOW_REMOVE_USER,
        UserManager.DISALLOW_SAFE_BOOT,
        UserManager.DISALLOW_SET_USER_ICON,
        UserManager.DISALLOW_SET_WALLPAPER,
        UserManager.DISALLOW_SHARE_LOCATION,
        UserManager.DISALLOW_SMS
//        UserManager.DISALLOW_UNINSTALL_APPS
//        UserManager.DISALLOW_USB_FILE_TRANSFER
    )

    var isAdmin = false

    private var devicePolicyManager: DevicePolicyManager = context.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager

    init {

        if (devicePolicyManager.isDeviceOwnerApp(context.packageName)) {
            Log.d(TAG, "Your app is the Device Owner.")
            isAdmin = true
        } else {
            Log.d(TAG, "Your app is NOT the Device Owner.")
        }
    }

    @Suppress("DEPRECATION")
    fun setDemoPolicies(adminComponent: ComponentName, enable: Boolean, activity: AppCompatActivity) {
        if (isAdmin) {
            //todo configable StayAwake
            setRestrictions(adminComponent, enable)
            setKeyGuardEnable(adminComponent, enable)
            setLockTask(adminComponent, enable, activity)
            setImmersiveMode(enable, activity)
            Log.d(TAG, "DemoPolicies are set...")
        }
    }

    private fun setRestrictions(adminComponent: ComponentName, disallow: Boolean) {
        defaultRestrictions.forEach {
            setUserRestriction(adminComponent, it, disallow)
        }
        Log.d(TAG, "Restrictions set...")
    }

    private fun setUserRestriction(adminComponent: ComponentName, restriction: String, disallow: Boolean) =
        if (disallow) {
            devicePolicyManager.addUserRestriction(adminComponent, restriction)
        } else {
            devicePolicyManager.clearUserRestriction(adminComponent, restriction)
        }

    private fun setKeyGuardEnable(adminComponent: ComponentName, enable: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            devicePolicyManager.setKeyguardDisabled(adminComponent, !enable)
        }
    }

    // For this need an activity to run LockTask Mode
    fun setLockTask(adminComponent: ComponentName, start: Boolean, activity: AppCompatActivity) {
        if (isAdmin) {
            devicePolicyManager.setLockTaskPackages(
                adminComponent,
                if (start) arrayOf(context.packageName) else arrayOf()
            )
        }
        if (start) {
            activity.startLockTask()
        } else {
            activity.stopLockTask()
        }
    }
//    fun setLockTaskFeature(adminName: ComponentName, flag :Int) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
//            devicePolicyManager.setLockTaskFeatures(adminName, flag )
//        }
//    }

    // For this need an activity to run LockTask Mode
    private fun setImmersiveMode(enable: Boolean, activity: AppCompatActivity) {
        if (enable) {
            val flags = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
            activity.window.decorView.systemUiVisibility = flags
        } else {
            val flags = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
            activity.window.decorView.systemUiVisibility = flags
        }
    }
}