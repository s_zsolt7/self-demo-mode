package info.wallboard.pinlockview

import info.wallboard.pinlockview.model.UnlockHandlerModel
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ModelUnitTest {
    private lateinit var key: String
    private lateinit var unlockHandlerModel: UnlockHandlerModel

    @Before
    fun setUp() {
        key = "0000"

        unlockHandlerModel = UnlockHandlerModel(key)
        println("Testing set up!")
    }

    @After
    fun finish() {
        println("Testing finished!")
    }

    @Test
    fun unacceptablePinCodeTest() {
        var a = 7
        assertTrue("True was excepted but this is false", unlockHandlerModel.addKeyCode(a))
        a = 8
        assertTrue("True was excepted but this is false", unlockHandlerModel.addKeyCode(a))
        a = 9
        assertTrue("True was excepted but this is false", unlockHandlerModel.addKeyCode(a))
        a = 10
        assertFalse("False was excepted but this is true", unlockHandlerModel.addKeyCode(a))

        assertFalse(unlockHandlerModel.isAccepted())
        println("Unacceptable pin code test passed!")
    }

    @Test
    fun acceptablePinCodeTest() {
        var pin = 7
        assertTrue("True was excepted but this is false", unlockHandlerModel.addKeyCode(pin))
        assertTrue("True was excepted but this is false", unlockHandlerModel.addKeyCode(pin))
        assertTrue("True was excepted but this is false", unlockHandlerModel.addKeyCode(pin))
        assertFalse("False was excepted but this is true", unlockHandlerModel.addKeyCode(pin))

        assertTrue(unlockHandlerModel.isAccepted())
        println("Acceptable pin code test passed!")
    }

    @Test
    fun acceptableKeyCodeTest() {
        var keyValue: Int = 7
        assertTrue(unlockHandlerModel.isKeyAcceptable(keyValue))
        keyValue = 9
        assertTrue(unlockHandlerModel.isKeyAcceptable(keyValue))
        keyValue = 13
        assertTrue(unlockHandlerModel.isKeyAcceptable(keyValue))
        keyValue = 16
        assertTrue(unlockHandlerModel.isKeyAcceptable(keyValue))
        println("Acceptable keys test passed")
    }

    @Test
    fun unacceptableKeyCodeTest() {
        var keyValue: Int = 6
        assertFalse(unlockHandlerModel.isKeyAcceptable(keyValue))
        keyValue = 17
        assertFalse(unlockHandlerModel.isKeyAcceptable(keyValue))
        keyValue = 22
        assertFalse(unlockHandlerModel.isKeyAcceptable(keyValue))
        keyValue = 1
        assertFalse(unlockHandlerModel.isKeyAcceptable(keyValue))
        println("Unacceptable keys test passed")
    }

    @Test
    fun emptyPinCodeTest() {
        assertTrue(unlockHandlerModel.isPinEmpty())
        println("The pin is empty right now.")
    }

    @Test
    fun addPinAndRemoveItThenEmptyTest() {
        assertTrue(unlockHandlerModel.addKeyCode(10))
        unlockHandlerModel.removeKeyCode()
        assertTrue(unlockHandlerModel.isPinEmpty())
        println("Added pin removed and the pincode is empty")
    }

    @Test
    fun addPinsAndClearThemThenEmptyTest() {
        assertTrue(unlockHandlerModel.addKeyCode(10))
        assertTrue(unlockHandlerModel.addKeyCode(11))
        assertTrue(unlockHandlerModel.addKeyCode(12))
        unlockHandlerModel.clearKeyCode()
        assertTrue(unlockHandlerModel.isPinEmpty())
        println("Added pins cleared and the pincode is empty")
    }
}