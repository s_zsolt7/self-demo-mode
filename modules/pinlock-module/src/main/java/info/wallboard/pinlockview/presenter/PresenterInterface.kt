package info.wallboard.pinlockview.presenter

import android.view.KeyEvent
import info.wallboard.pinlockview.callbacks.PinLockCallback
import info.wallboard.pinlockview.view.views.ButtonGroupView
import info.wallboard.pinlockview.view.views.IndicatorDotView

interface PresenterInterface {
    fun setViews(pinLock: PinLockCallback, indicatorDot: IndicatorDotView, buttonGroup: ButtonGroupView)
    fun destructor()
    fun handleKeyEvent(event: KeyEvent): KeyEvent
    fun addPin(value: String): Boolean
    fun addPin(value: Int): Boolean
    fun removePin()
    fun destroyPinLock()
    fun delayedCheck()
}