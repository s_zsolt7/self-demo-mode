package info.wallboard.pinlockview.view


import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button
import androidx.constraintlayout.widget.ConstraintLayout
import info.wallboard.pinlockview.presenter.PresenterInterface
import info.wallboard.pinlockview.view.views.ButtonGroupView
import kotlinx.android.synthetic.main.pinlock_layout.view.*

class ButtonGroup @JvmOverloads constructor(context: Context, attributeSet: AttributeSet? = null) : ConstraintLayout(context, attributeSet), ButtonGroupView {
    private var presenter: PresenterInterface? = null

    private val pinValueBtnListener = OnClickListener { v ->
        presenter?.addPin((v as Button).text.toString())
    }
    private val backspaceBtnListener = OnClickListener { presenter?.removePin() }
    private val destroyerCloseBtnListener = OnClickListener { presenter?.destroyPinLock() }

    fun setPresenter(presenter: PresenterInterface) {
        this.presenter = presenter
        pinlock_button_backspace.setOnClickListener(backspaceBtnListener)
        pinlock_button_close.setOnClickListener(destroyerCloseBtnListener)
        setValueBtnListener(pinValueBtnListener)
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        return super.dispatchKeyEvent(presenter?.handleKeyEvent(event))
    }

    private fun setValueBtnListener(onClickListener: OnClickListener) {
        pinlock_numberbtn_no0.setOnClickListener(onClickListener)
        pinlock_numberbtn_no1.setOnClickListener(onClickListener)
        pinlock_numberbtn_no2.setOnClickListener(onClickListener)
        pinlock_numberbtn_no3.setOnClickListener(onClickListener)
        pinlock_numberbtn_no4.setOnClickListener(onClickListener)
        pinlock_numberbtn_no5.setOnClickListener(onClickListener)
        pinlock_numberbtn_no6.setOnClickListener(onClickListener)
        pinlock_numberbtn_no7.setOnClickListener(onClickListener)
        pinlock_numberbtn_no8.setOnClickListener(onClickListener)
        pinlock_numberbtn_no9.setOnClickListener(onClickListener)
    }

    override fun setBtnEnable(value: Boolean) {
//        if (!value) isThereNext = false
        var child: View
        for (i in 0 until childCount) {
            child = getChildAt(i) as Button
            child.isEnabled = value
        }
    }
    override fun hideBackspaceBtn() {
        pinlock_button_backspace.visibility = View.GONE
    }
    override fun showBackspaceBtn() {
        pinlock_button_backspace.visibility = View.VISIBLE
    }
}








