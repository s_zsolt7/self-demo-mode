package info.wallboard.pinlockview.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.res.ResourcesCompat
import info.wallboard.pinlockview.R
import info.wallboard.pinlockview.view.views.IndicatorDotView
import java.util.*

class IndicatorDot @JvmOverloads constructor(context: Context, attributeSet: AttributeSet? = null) : LinearLayout(context, attributeSet), IndicatorDotView {

    private val dotList = ArrayList<View>()
    private val params = LayoutParams(10, 10).also { it.setMargins(10, 0, 10, 0) }

    override fun append() {
        val dot = View(context)
        dot.background = ResourcesCompat.getDrawable(resources, R.drawable.dot_filled, null)
        dot.layoutParams = params
        dot.id = View.generateViewId()
        addView(dot)
        dotList.add(dot)
    }

    override fun remove() {
        if (dotList.isNotEmpty()) {
            val removableDot = dotList[dotList.size - 1]
            removeView(removableDot)
            dotList.remove(removableDot)
        }
    }

    override fun clear() {
        dotList.clear()
        removeAllViews()
    }

}
