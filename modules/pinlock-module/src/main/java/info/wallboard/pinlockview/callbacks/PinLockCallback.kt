package info.wallboard.pinlockview.callbacks

interface PinLockCallback {
    fun onPinChecked(isAccepted: Boolean)
    fun onClose()
    fun delayedRunnable(runnable: Runnable, delay: Long)
}