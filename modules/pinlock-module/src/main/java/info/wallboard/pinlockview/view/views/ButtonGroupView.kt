package info.wallboard.pinlockview.view.views

interface ButtonGroupView {
    fun setBtnEnable(value: Boolean)
    fun showBackspaceBtn()
    fun hideBackspaceBtn()
}