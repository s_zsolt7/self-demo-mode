package info.wallboard.pinlockview.view.views

interface IndicatorDotView {
        fun append()
        fun remove()
        fun clear()
    }