package info.wallboard.pinlockview.model

interface UnlockInterface {
    fun isKeyAcceptable(value: Int): Boolean
    fun addKeyCode(keyCode: String): Boolean
    fun addKeyCode(keyCode: Int): Boolean
    fun removeKeyCode()
    fun clearKeyCode()
    fun isAccepted(): Boolean
    fun isPinEmpty(): Boolean

}