package info.wallboard.pinlockview.model

import android.util.Log
import android.view.KeyEvent
import java.io.IOException
import java.util.regex.Pattern
import android.view.KeyCharacterMap
import java.lang.IllegalArgumentException


class UnlockHandlerModel(pin: String): UnlockInterface {

    companion object {
        @JvmStatic
        val mKeyCharacterMap: KeyCharacterMap? = KeyCharacterMap.load(KeyCharacterMap.VIRTUAL_KEYBOARD)

        @JvmStatic
        val acceptedKey = mapOf(
                KeyEvent.KEYCODE_0 to "0",
                KeyEvent.KEYCODE_1 to "1",
                KeyEvent.KEYCODE_2 to "2",
                KeyEvent.KEYCODE_3 to "3",
                KeyEvent.KEYCODE_4 to "4",
                KeyEvent.KEYCODE_5 to "5",
                KeyEvent.KEYCODE_6 to "6",
                KeyEvent.KEYCODE_7 to "7",
                KeyEvent.KEYCODE_8 to "8",
                KeyEvent.KEYCODE_9 to "9"
        )
    }

    private var keyCodeList: ArrayList<String> = ArrayList()
    private var ourCodeList: ArrayList<String> = ArrayList()
    private var idx: Int = 0
    private var accepted = true

    init {
        if (isAccessPinValid(pin)) {
            Log.d("Accept_PIN", ": $pin")
            pin.forEach {
                keyCodeList.add(it.toString())
            }
            Log.d("Access_PIN_List", "s: $keyCodeList")
        } else {
            throw IOException("Access Pin is NOT acceptable!")
        }
    }

    private fun isAccessPinValid(pin: String): Boolean {
        return Pattern.compile("([0-9]{4,6})").matcher(pin).matches().also { Log.d("VALIDATION", "PinCode is VALID! :)") }
    }

    override fun isKeyAcceptable(value: Int): Boolean {
        return acceptedKey[value] != null
    }

    override fun addKeyCode(keyCode: String): Boolean {
        return if (mKeyCharacterMap != null) {
            addKeyCode(mKeyCharacterMap.getEvents(keyCode.toCharArray())[0].keyCode)
        } else {
            false
        }
    }

    override fun addKeyCode(keyCode: Int): Boolean {
        val res = acceptedKey[keyCode]
        if (idx < keyCodeList.size) {
            res?.run {
                ourCodeList.add(this)
                Log.d("Listcheck","$ourCodeList")
                if (keyCodeList[idx] != ourCodeList[idx]) {
                    accepted = false
                    Log.d("Accepted_Set_To", ": $accepted")
                }
                idx++
                return idx < keyCodeList.size
            } ?: {
                throw IllegalArgumentException("Not acceptable Input")
            } ()
        }
        return false
    }

    override fun removeKeyCode() {
        var correct = 0
        Log.d("Listcheck","$ourCodeList")
        ourCodeList.removeAt(idx-1)
        idx--
        for (i in 1..idx) {
            if (keyCodeList[i-1] == ourCodeList[i-1]) {
                correct++
            } else {
                if (accepted) {
                    accepted = false
                    Log.d("Accepted_Set_To", ": $accepted")
                }
            }
        }
            Log.d("Correct", ": $correct")
        if (correct == idx) {
            accepted = true
            Log.d("Accepted_Set_To", ": $accepted")
        }
    }

    override fun clearKeyCode() {
        idx = 0
        ourCodeList.clear()
        accepted = true
    }

    override fun isAccepted(): Boolean {
        return accepted && idx == keyCodeList.size
    }

    override fun isPinEmpty(): Boolean {
        Log.d("MY PINS", "$ourCodeList")
        return ourCodeList.isEmpty()
    }
}