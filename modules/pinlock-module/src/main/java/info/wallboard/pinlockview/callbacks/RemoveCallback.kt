package info.wallboard.pinlockview.callbacks

import info.wallboard.pinlockview.view.PinLock

interface RemoveCallback {
    fun onRemove(pinLockView: PinLock)
    fun onAccess(pinLockView: PinLock)
}
