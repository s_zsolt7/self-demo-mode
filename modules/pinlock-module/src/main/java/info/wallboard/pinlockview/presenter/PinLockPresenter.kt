package info.wallboard.pinlockview.presenter

import android.util.Log
import android.view.KeyEvent
import info.wallboard.pinlockview.model.UnlockInterface
import info.wallboard.pinlockview.callbacks.PinLockCallback
import info.wallboard.pinlockview.model.UnlockHandlerModel
import info.wallboard.pinlockview.view.views.ButtonGroupView
import info.wallboard.pinlockview.view.views.IndicatorDotView

class PinLockPresenter(accessPin: String) : PresenterInterface {

    private companion object {
        private const val DELAY = 1000L
    }

    private var unlockInterface: UnlockInterface = UnlockHandlerModel(accessPin)
    private var pinLock: PinLockCallback? = null
    private var indicatorDot: IndicatorDotView? = null
    private var buttonGroup: ButtonGroupView? = null

    private var isThereNext: Boolean = true
    private val delayedCheckRunnable: Runnable by lazy {
        Runnable {
            delayedCheck()
        }
    }

    override fun setViews(pinLock: PinLockCallback, indicatorDot: IndicatorDotView, buttonGroup: ButtonGroupView) {
        this.pinLock = pinLock
        this.indicatorDot = indicatorDot
        this.buttonGroup = buttonGroup
    }

    override fun destructor() {
        this.pinLock = null
        this.indicatorDot = null
        this.buttonGroup = null
        this.unlockInterface.clearKeyCode()
    }
    override fun addPin(value: String): Boolean {
        isThereNext = unlockInterface.addKeyCode(value)
        afterAddPin()
        return isThereNext
    }
    override fun addPin(value: Int): Boolean {
        isThereNext = unlockInterface.addKeyCode(value)
        afterAddPin()
        return isThereNext
    }

    override fun handleKeyEvent(event: KeyEvent): KeyEvent {
        if (event.action == KeyEvent.ACTION_DOWN) {
            if (event.keyCode == KeyEvent.KEYCODE_DEL) {
                if (!unlockInterface.isPinEmpty()) {
                    removePin()
                }
            } else if (event.keyCode == KeyEvent.KEYCODE_ESCAPE) {
                pinLock?.onClose()
            } else if (unlockInterface.isKeyAcceptable(event.keyCode)) {
                if (isThereNext) {
                    isThereNext = unlockInterface.addKeyCode(event.keyCode)
                    Log.d("IsThereNext", ": $isThereNext")
                    afterAddPin()
                }
            } else {
                return event
            }
        }
        return event
    }
    private fun afterAddPin() {
        indicatorDot?.append()
        if (!isThereNext) {
            buttonGroup?.setBtnEnable(false)
            pinLock?.delayedRunnable(delayedCheckRunnable, DELAY)
        }
        if (!unlockInterface.isPinEmpty()) {
            buttonGroup?.showBackspaceBtn()
        }
    }
    override fun removePin() {
        indicatorDot?.remove()
        unlockInterface.removeKeyCode()
        if (unlockInterface.isPinEmpty()) {
            buttonGroup?.hideBackspaceBtn()
        }
    }
    override fun destroyPinLock() {
        pinLock?.onClose()
    }

    override fun delayedCheck() {
        if (unlockInterface.isAccepted()) {
            Log.d("ACCESS", ": VICTORYYYYYYYYYYY")
            pinLock?.onPinChecked(true)
        } else {
            Log.d("ERROR", "Check pin try: FAIL")
            buttonGroup?.setBtnEnable(true)
            unlockInterface.clearKeyCode()
            buttonGroup?.hideBackspaceBtn()
            indicatorDot?.clear()
            isThereNext = true
            pinLock?.onPinChecked(false)
        }
    }
}