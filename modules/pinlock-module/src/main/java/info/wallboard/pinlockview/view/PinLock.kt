package info.wallboard.pinlockview.view

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.constraintlayout.widget.ConstraintLayout
import info.wallboard.pinlockview.R
import info.wallboard.pinlockview.callbacks.PinLockCallback
import info.wallboard.pinlockview.callbacks.RemoveCallback
import info.wallboard.pinlockview.presenter.PinLockPresenter
import info.wallboard.pinlockview.presenter.PresenterInterface
import kotlinx.android.synthetic.main.pinlock_layout.view.*

class PinLock(context: Context, accessPin: String, private val removeCallback: RemoveCallback) : ConstraintLayout(context), PinLockCallback {

    //presenter
    private var presenter: PresenterInterface


    private var shakeAnimation: Animation
    private val shakeAnimationListener = object : Animation.AnimationListener {
        override fun onAnimationStart(animation: Animation) {
            pinlock_button_group_layout.isEnabled = false
        }

        override fun onAnimationEnd(animation: Animation) {
            pinlock_button_group_layout.hideBackspaceBtn()
            pinlock_indicator_dot_layout.clear()
            pinlock_button_group_layout.isEnabled = true
        }

        override fun onAnimationRepeat(animation: Animation) { /*  unused :(  */
        }
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.pinlock_layout, this)

        presenter = PinLockPresenter(accessPin)
        presenter.setViews(this, pinlock_indicator_dot_layout, pinlock_button_group_layout)
        pinlock_button_group_layout.setPresenter(presenter)

        shakeAnimation = AnimationUtils.loadAnimation(context, R.anim.shake)
        shakeAnimation.setAnimationListener(shakeAnimationListener)
    }

    override fun onPinChecked(isAccepted: Boolean) {
        Log.d("ASD", "CHEKINGTHISSHIEEEET $isAccepted")
        if (isAccepted) {
            presenter.destructor()
            removeCallback.onAccess(this)
        } else {
            Log.d("FALSE", "SHAKELJMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAr")
            pinlock_button_group_layout.startAnimation(shakeAnimation)
        }
    }

    override fun onClose() {
        presenter.destructor()
        removeCallback.onRemove(this)
    }

    override fun delayedRunnable(runnable: Runnable, delay: Long) {
        postDelayed(runnable, delay)
    }
}

